/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.borjabravo.readmoretextview;

import ohos.app.Context;

import java.util.logging.Logger;

public class Utils {
    /**
     * isEmpty
     *
     * @param content content
     * @return isEmpty
     */
    public static boolean isEmpty(String content) {
        return content == null || "".equals(content);
    }

    /**
     * px2vp
     *
     * @param context context
     * @param pxValue pxValue
     * @return int
     */
    public static int px2vp(Context context, final float pxValue) {
        final float scale = context.getResourceManager().getDeviceCapability().screenDensity;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * vp2px
     *
     * @param context context
     * @param dpValue dpValue
     * @return int
     */
    public static int vp2px(Context context, final float dpValue) {
        final float scale = context.getResourceManager().getDeviceCapability().screenDensity;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * log
     *
     * @param msg msg
     */
    public static void log(String msg) {
        Logger.getGlobal().info("jwen====" + msg);
    }


}
