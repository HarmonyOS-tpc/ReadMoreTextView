/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.borjabravo.readmoretextviewsample.slice;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.borjabravo.readmoretextviewsample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        ReadMoreTextView text1 = (ReadMoreTextView) findComponentById(ResourceTable.Id_text1);
        text1.setContent("Blockly拥有大量预定义的块，从数学函数导循环结构等应有尽有。但是有时候我们仍然需要自定义块以符合我们个性化的需求。在通常情况下，自定义块最快的方法就是找到一个最符合需求并且已经存在的块，在已有基础上对其进行修改。");
        ReadMoreTextView text2 = (ReadMoreTextView) findComponentById(ResourceTable.Id_text2);
        text2.setContent("Blockly拥有大量预定义的块，从数学函数导循环结构等应有尽有。但是有时候我们仍然需要自定义块以符合我们个性化的需求。在通常情况下，自定义块最快的方法就是找到一个最符合需求并且已经存在的块，在已有基础上对其进行修改。");
        ReadMoreTextView text3 = (ReadMoreTextView) findComponentById(ResourceTable.Id_text3);
        text3.setContent("但是有时候我们仍然需要自定义块以符合我们个性化的需求。在通常情况下。");

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
